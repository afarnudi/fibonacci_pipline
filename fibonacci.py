"""
Your module doc goes here.

Add a couple of lines for the description as well.
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-


def fibonacci(n):
    """
    Calculate numbers in the Fibonacci sequence.

    Parameters
    ----------
    n : int
        The position of the number in the sequence.

    Returns
    -------
    result : int
        The n'th number in the Fibonacci sequence.

    """
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    result = a
    return result
