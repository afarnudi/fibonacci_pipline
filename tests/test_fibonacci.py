import sys
module_path = "../" #or “../.."
if module_path not in sys.path:
   sys.path.append(module_path)

from fibonacci import fibonacci

def test_fibonacci_first_seq():
    assert fibonacci(1) == 1
